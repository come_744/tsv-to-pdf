# TSV to PDF

**Convert `.tsv` files from Framafoms to PDF documents**


## Requirements

* `python3`
* `Pandoc` with `latex tikz`
* `xdg-open`


## Use it

To make it simple:

* `git clone` this repo
* Add your `*.tsv` file into the created directory
* Open a terminal into the directory
* Run `./tsv_to_pdf <my tsv file>`
* Follow the configuration instructions (if you don't know what to do just press
  `ENTER`, the default parameters should be OK)
* When you are asked to fill the configuration file, open it and replace `"t"`
  by `"r"` when the field is a group of radio buttons.


## Grouping

When you group on a field F, each other field will display its values grouped by
values of F.

For instance, if your form is a survey for a school class split into groups GI
and GII, and you want to get the results of the survey for each group, add a
required _radio_ field with values "GI" and "GII" in Framaforms when you create
the form. Then, to process the results with this script, set the flags of the
field "group" to `"g"` in the configuration file. Then all the fields in the PDF
file will be separated: for each field, all the values of group GI will be
printed before those of group GII.

Notice that if you have _radio_ fields (flag `"r"`) and you group on other
_radio_ fields, the statistics will be made both globally and per group.

If you want a specific field not to be grouped, add flag `n` to it.
